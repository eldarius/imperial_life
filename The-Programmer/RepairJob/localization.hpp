class Repair_Job_Localization {
	class STR_LEAVE_JOB {
		en = "Jobs <br/>You have just left your job as a repairman.";
		fr = "Pole Emploie <br/>Tu viens de quitter ton métier d'agent de réparation.";
		de = "Jobs <br/>Du hast den Job als Mechaniker verlassen.";
	};
	class STR_BECAME_REPAIR_MAN {
		en = "Jobs <br/>You have become a repairman, you can now go around the map by repairing objects.";
		fr = "Pole Emploie <br/>Vous êtes devenu agent de réparation, vous pouvez maintenant faire le tour de la carte en reparant des objets.";
		de = "Jobs <br/>Du bist nun ein Mechaniker, lauf umher und Repariere kaputte Objekte.";
	};
	class STR_MONEY_RECEIVE {
		en = "You received $%1 for reparing one object";
		fr = "Tu viens de reçevoir %1 € pour avoir reparé un objet.";
		de = "Du erhälst $%1 für das Reparieren eines Objektes.";
	};
	class STR_CANT_REPAIR {
		en = "You can only repair object every %1 seconds !";
		fr = "Tu ne peux que réparer des objets que toute les %1 secondes !";
		de = "Du kannst nur alle %1 Objekte Reparieren !";
	};
	class STR_Nearby_Objects {
		en = "Repairing object...";
		fr = "Réparation de l'objet...";
		de = "Repariere Objekt...";
	};
	class STR_NOT_ACCES {
		en = "You are not allowed to become a repairman !";
		fr = "Vous n'avez pas les permissions requises pour devenir agent de réparation !";
		de = "Du bist nicht dazu berechtigt ein Mechaniker zu werden !";
	};
	class STR_NOT_LICENCE {
		en = "You do not have the required licence !";
		fr = "Vous n'avez pas la licence requise !";
		de = "Du hast nicht die benötigte Lizens !";
	};
	class STR_REPAIR_MENU {
		en = "Job Center : Repair Man";
		fr = "Job Center : Agent de réparation";
		de = "Job Center : Mechaniker";
	};
};