class Market
{
    idd = 39000;
    name = "Market";
    movingEnable = false;
    enableSimulation = true;

    class controlsBackground
    {
        class TitleBackground: Life_RscText
        {
            idc = 1000;
            x = SafeZoneX + (669.999999999999 / 1920) * SafeZoneW;
			y = SafeZoneY + (312.999999999999 / 1080) * SafeZoneH;
			w = (494.999999999999 / 1920) * SafeZoneW;
			h = (16.0000000000001 / 1080) * SafeZoneH;
            colorBackground[] = {0.05,0.5,0.05,0.8};
            colorActive[] = {0.05,0.5,0.05,0.8};
        };
        class MarketBackground: Life_RscText
        {
            idc = 1001;
            x = SafeZoneX + (669.999999999999 / 1920) * SafeZoneW;
			y = SafeZoneY + (330.833333333333 / 1080) * SafeZoneH;
			w = (494.999999999999 / 1920) * SafeZoneW;
			h = (315.999999999999 / 1080) * SafeZoneH;
            colorBackground[] = {0,0,0,0.6};
        };
        class Title: Life_RscText
        {
            idc = 1002;
            text = "Economie de l'île d'Altis"; //--- ToDo: Localize;
            x = SafeZoneX + (669.999999999999 / 1920) * SafeZoneW;
			y = SafeZoneY + (312.999999999999 / 1080) * SafeZoneH;
			w = (494.999999999999 / 1920) * SafeZoneW;
			h = (16.0000000000001 / 1080) * SafeZoneH;
        };
    };

    class Controls
    {
        class GoodsDisplay: Life_RscListbox
        {
            idc = 39001;
            text = "";
            sizeEx = 0.035;
            onLBSelChanged = "[] spawn life_fnc_marketGrab";
            x = SafeZoneX + (683.333333333333 / 1920) * SafeZoneW;
			y = SafeZoneY + (342.5 / 1080) * SafeZoneH;
			w = (465 / 1920) * SafeZoneW;
			h = (290.000000000001 / 1080) * SafeZoneH;
        };
       
        class ExitButton: Life_RscButtonMenu
        {
            idc = 39008;
            text = "$STR_Global_Close";
            onButtonClick = "closeDialog 0;";
            x = SafeZoneX + (1083 / 1920) * SafeZoneW;
			y = SafeZoneY + (646.166666666667 / 1080) * SafeZoneH;
			w = (80.9999999999999 / 1920) * SafeZoneW;
			h = (20 / 1080) * SafeZoneH;
            colorBackground[] = {0.05,0.5,0.05,0.8};
            colorActive[] = {0.05,0.5,0.05,0.8};
        };
    };
};