/*
*    FORMAT:
*        STRING (Conditions) - Must return boolean :
*            String can contain any amount of conditions, aslong as the entire
*            string returns a boolean. This allows you to check any levels, licenses etc,
*            in any combination. For example:
*                "call life_coplevel && license_civ_someLicense"
*            This will also let you call any other function.
*            
*
*    ARRAY FORMAT:
*        0: STRING (Classname): Item Classname
*        1: STRING (Nickname): Nickname that will appear purely in the shop dialog
*        2: SCALAR (Buy price)
*        3: SCALAR (Sell price): To disable selling, this should be -1
*        4: STRING (Conditions): Same as above conditions string
*
*    Weapon classnames can be found here: https://community.bistudio.com/wiki/Arma_3_CfgWeapons_Weapons
*    Item classnames can be found here: https://community.bistudio.com/wiki/Arma_3_CfgWeapons_Items
*
*/
class WeaponShops {
    //Armory Shops
    class gun {
        name = "Billy Joe's Firearms";
        side = "civ";
        conditions = "license_civ_gun";
        items[] = {
            { "TFAR_anprc152", "", 10, 10, ""},
            { "SWOP_DL18Pistol", "", 6500, 3250, "" },
            { "SWOP_rg4dPistol", "", 7000, 3500, "" },
            { "SWOP_DL44Pistol", "", 9850, 4925, "" },
            { "SWOP_Westar35Pistol", "", 11500, 5750, "" },
            { "SWOP_EE4BlasterRifle", "", 18000, 9000, "" }, 
            { "relbyv10", "", 20000, 10000, "" }
        };
        mags[] = {
            { "SWOP_DL18Pistol_Mag", "", 125, 60, "" },
            { "SWOP_rg4dPistol_Mag", "", 150, 75, "" },
            { "SWOP_DL44Pistol_Mag", "", 200, 100, "" },
            { "SWOP_Westar35Pistol_Mag", "", 250, 125, "" },
            { "SWOP_EE4BlasterRifle_Mag", "", 250, 125, "" }, 
            { "relbyv10_Mag", "", 250, 125, "" } 
        };
        accs[] = {
            { "swop_ee4blaster_scope", "", 2500, 1250, "" }
        };
    };

    class rebel {
        name = "Mohammed's Jihadi Shop";
        side = "civ";
        conditions = "license_civ_rebel";
        items[] = {
            { "SWOP_A280A", "", 27500, 13500, "" },
            { "SWOP_A300SBlasterRifle", "", 50000, 25000, "" },
            { "SWOP_E5", "", 20000, 10000, "" },
            { "SWOP_DH17BlasterRifle", "", 22000, 11000, "" }, 
            { "SWOP_A180BlasterRifle", "", 25000, 12500, "" }, 
            { "SWOP_DC15", "", 25000, 12500, "" }, 
            { "SWOP_dc15xBlasterRifle", "", 50000, 25000, "" }, 
            { "SWOP_DC15ABlasterRifle", "", 32000, 16000, "" }, 
            { "SWOP_A280C_MOD1", "", 35000, 17000, "" },
            { "TFAR_anprc152", "", 10, 10, ""}
        };
        mags[] = {
            { "SWOP_A180BlasterRifle_Mag", "", 300, 150, "" },
            { "SWOP_dc15xBlasterRifle_Mag", "", 500, 250, "" }, 
            { "SWOP_A280BlasterRifle_Mag", "", 350, 170, "" },
            { "SWOP_DC15ABlasterRifle_Full_Mag", "", 320, 160, "" },
            { "SWOP_A180BlasterRifle_Mag", "", 275, 140, "" }, 
            { "SWOP_DC15_Mag", "", 275, 140, "" },
            { "SWOP_A300SBlasterRifle_Mag", "", 500, 250, "" },
            { "SWOP_E5_Mag", "", 125, 60, "" },
            { "SWOP_DH17BlasterRifle_Mag", "", 125, 60, "" } 
        };
        accs[] = {
            { "swop_dh17blaster_scope", "", 3500, 1750, "" }
        };
    };

    class gang {
        name = "Hideout Armament";
        side = "civ";
        conditions = "";
        items[] = {
            { "SWOP_DC17Pistol", "", 20000, 6000, "" },
            { "SWOP_CR2BlasterRifle", "", 30000, 10000, "" },
            { "SW_cj9_BlasterRifle", "", 40000, 12500, "" },
            { "TFAR_anprc152", "", 10, 10, ""}
        };
        mags[] = {
            { "SW_cj9_BlasterRifle_Mag", "", 1500, 600, "" },
            { "SWOP_DC17Pistol_Mag", "", 800, 300, "" },
            { "SWOP_CR2_Blaster_Mag", "", 1000, 400, "" }
         };
    };

    //Basic Shops
    class genstore {
        name = "Altis General Store";
        side = "civ";
        conditions = "";
        items[] = {
            { "Binocular", "", 150, 75, "" },
            { "ItemGPS", "", 100, 50, "" },
            { "ItemMap", "", 50, 25, "" },
            { "ItemCompass", "", 50, 25, "" },
            { "ItemWatch", "", 50, 25, "" },
            { "FirstAidKit", "", 150, 75, "" },
            { "NVGoggles", "", 2000, 1000, "" },
            { "Chemlight_red", "", 300, 150, "" },
            { "Chemlight_yellow", "", 300, 150, "" },
            { "Chemlight_green", "", 300, 150, "" },
            { "Chemlight_blue", "", 300, 150, "" },
            { "TFAR_anprc152", "", 10, 10, "" }
        };
        mags[] = {};
        accs[] = {};
    };

    class f_station_store {
        name = "Altis Fuel Station Store";
        side = "";
        conditions = "";
        items[] = {
            { "Binocular", "", 750, 75, "" },
            { "ItemGPS", "", 500, 50, "" },
            { "ItemMap", "", 250, 25, "" },
            { "ItemCompass", "", 250, 25, "" },
            { "ItemWatch", "", 250, 25, "" },
            { "FirstAidKit", "", 750, 75, "" },
            { "NVGoggles", "", 10000, 1000, "" },
            { "Chemlight_red", "", 1500, 150, "" },
            { "Chemlight_yellow", "", 1500, 150, "" },
            { "Chemlight_green", "", 1500, 150, "" },
            { "Chemlight_blue", "", 1500, 150, "" },
            { "TFAR_anprc152", "", 10, 10, "" }
        };
        mags[] = {};
        accs[] = {};
    };

    //Cop Shops
    class cop_basic {
        name = "Altis Cop Shop";
        side = "cop";
        conditions = "";
        items[] = {
            { "ElectroBinocularsW_F", "", 0, 0, "" },
            { "ItemGPS", "", 0, 0, "" },
            { "FirstAidKit", "", 0, 0, "" },
            { "SWOP_NVChipClean", "", 0, 0, "" },
            { "HandGrenade_Stone", $STR_W_items_Flashbang, 0, 0, "" },
            { "hgun_P07_snds_F", $STR_W_items_StunPistol, 0, 0, "" },
            { "arifle_SDAR_F", $STR_W_items_TaserRifle, 0, 0, "" },
            { "SWOP_rk3", "", 0, 0, "" },
            { "swop_ScoutPistol", "", 0, 0, "" },
            { "SWOP_e11", "", 0, 0, "" },
            { "swop_e11b", "", 3000, 1500, "call life_coplevel >= 2" },
            { "SWOP_T21BlasterRifle", "", 37000, 18500, "call life_coplevel >= 3" },
            { "SWOP_DLT19DBlasterRifle", "", 40000, 20000, "call life_coplevel >= 3" },
            { "TFAR_anprc152", "", 10, 10, "" }
        };
        mags[] = {
            { "SWOP_rk3_Mag", "", 0, 0, "" },
            { "20Rnd_556x45_UW_mag", $STR_W_mags_TaserRifle, 0, 0, "" },
            { "SWOP_scoutPistol_Mag", "", 0, 0, "" },
            { "SWOP_e11_Mag", "", 0, 0, "" },
            { "swop_e11b_Mag", "", 150, 75, "call life_coplevel >= 2" },
            { "SWOP_T21BlasterRifle_Mag", "", 200, 100, "call life_coplevel >= 3" },
            { "SWOP_DLT19DBlasterRifle_Mag", "", 200, 100, "call life_coplevel >= 3" } 
        };
        accs[] = {
            { "swop_e11_bl_scope", "", 0, 0, "" },
        };
    };

    //Medic Shops
    class med_basic {
        name = "store";
        side = "med";
        conditions = "";
        items[] = {
            { "ItemGPS", "", 100, 50, "" },
            { "Binocular", "", 150, 75, "" },
            { "FirstAidKit", "", 150, 75, "" },
            { "NVGoggles", "", 1200, 600, "" },
            { "TFAR_anprc152", "", 10, 10, ""}
        };
        mags[] = {};
        accs[] = {};
    };
};
